import { getTimeToLocalTime } from './dataUtils';
import { BasicLogger } from '../interface/logger/basicLogger';

export class ConsoleLogger implements BasicLogger {

  private emitLogMessage(logLevel: 'debug' | 'error' | 'info' | 'log' | 'warn', primaryMessage: string, optionalSupportData: any[]) {
    console[logLevel](getTimeToLocalTime(),`:::${logLevel}:::`, primaryMessage, optionalSupportData)
  }
  
  public debug(primaryMessage: string, ...optionalSupportData: any[]): void {
    this.emitLogMessage('debug', primaryMessage, optionalSupportData);
  }

  public error(primaryMessage: string, ...optionalSupportData: any[]): void {
    this.emitLogMessage('error', primaryMessage, optionalSupportData);
  }

  public info(primaryMessage: string, ...optionalSupportData: any[]): void {
    this.emitLogMessage('info', primaryMessage, optionalSupportData);
  }

  public log(primaryMessage: string, ...optionalSupportData: any[]): void {
    this.emitLogMessage('log', primaryMessage, optionalSupportData);
  }

  public warn(primaryMessage: string, ...optionalSupportData: any[]): void {
    this.emitLogMessage('warn', primaryMessage, optionalSupportData);
  }
};